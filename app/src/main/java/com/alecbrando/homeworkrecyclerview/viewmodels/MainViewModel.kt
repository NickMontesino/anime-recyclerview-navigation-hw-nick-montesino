package com.alecbrando.homeworkrecyclerview.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alecbrando.homeworkrecyclerview.model.Repo
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.alecbrando.homeworkrecyclerview.util.ResourceWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    val repo by lazy { Repo() }

    private val _animes: MutableLiveData<ResourceWrapper> = MutableLiveData()
    val animes: LiveData<ResourceWrapper> get() = _animes

    fun grabAnimes(data: Animes) = viewModelScope.launch(Dispatchers.Main) {
        val response = repo.bundleAnimes(data)
        _animes.value = response
    }

}