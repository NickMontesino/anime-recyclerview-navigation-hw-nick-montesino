package com.alecbrando.homeworkrecyclerview.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.alecbrando.homeworkrecyclerview.databinding.FragmentListBinding
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.util.ResourceWrapper
import com.alecbrando.homeworkrecyclerview.util.getJsonDataFromAsset
import com.alecbrando.homeworkrecyclerview.viewmodels.MainViewModel
import com.bumptech.glide.load.engine.Resource
import com.google.gson.Gson

class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val vm by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = getJsonDataFromAsset(requireContext(), "anime.json")
        val actualData = Gson().fromJson(data, Animes::class.java)
        vm.grabAnimes(actualData)
        initObservers()
    }

    private fun initObservers() = with(vm) {
        animes.observe(viewLifecycleOwner) { viewState ->
            when(viewState) {
                ResourceWrapper.Loading -> {

                }
                is ResourceWrapper.Success -> {
                    binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
                    binding.recyclerView.adapter = LovelyAdapter().apply {
                        applyAnimes(viewState.data)
                    }
                }
            }
        }
    }

}