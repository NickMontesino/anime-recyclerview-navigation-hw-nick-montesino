package com.alecbrando.homeworkrecyclerview.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.alecbrando.homeworkrecyclerview.databinding.FragmentDetailBinding
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.bumptech.glide.Glide

class DetailFragment: Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding: FragmentDetailBinding get() = _binding!!

    val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {
        super.onViewCreated(view, savedInstanceState)
        animeDetailsPoster.loadImage(args.anime.attributes.posterImage.large)
        animeDetailsNameAndRating.text = "${args.anime.attributes.canonicalTitle} - ${args.anime.attributes.averageRating}"
        animeDetailsSynopsis.text = args.anime.attributes.synopsis
    }

}