package com.alecbrando.homeworkrecyclerview.model

import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.alecbrando.homeworkrecyclerview.util.ResourceWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class Repo {

    private val apiDataRetriever = object : API {
        override suspend fun fetchAnimes(animes: Animes): List<Data> {
            return animes.data
        }
    }

    suspend fun bundleAnimes(data: Animes): ResourceWrapper = withContext(Dispatchers.IO) {
        return@withContext ResourceWrapper.Success(apiDataRetriever.fetchAnimes(data))
    }

}