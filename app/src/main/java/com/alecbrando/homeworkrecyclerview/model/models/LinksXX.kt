package com.alecbrando.homeworkrecyclerview.model.models

data class LinksXX(
    val related: String,
    val self: String
)