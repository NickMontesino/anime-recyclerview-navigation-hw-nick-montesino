package com.alecbrando.homeworkrecyclerview.model.models

data class LinksXXX(
    val related: String,
    val self: String
)